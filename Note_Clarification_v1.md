# Note de clarification - Projet Clinique Vétérinaire

Groupe : GHO Clément, JOY Matthew, LE MOUEL Yacine

# Justification du MCD :

## Personne

```
- Possède un nom et un prenom
- Abstraite car n'existe pas seule
```

## Client

```
- Est une personne (héritage).
- Est également un propriétaire d'animaux (voir clarification sur la classe Animal) ce qui explique la présence
  du champ "téléphone".
```

## Employé

```
- Est une personne (héritage)
- Possède un id qui l'identifie (clé)
```

## Vétérinaire

```
- Est une personne (héritage)
- Possède un id qui l'identifie (clé)
```

## Animal

```
- Possède un code, un nom, un poids, un genre, une date de naissance et une taille
- Le code et la taille ne sont pas obligatoires (attributs optionnels)
- Le genre est un type énuméré car fixe (Male ou Femelle)

- Possède un propriétaire qui est un Client. L'énoncé ne dit pas explicitement qu'un propriétaire est forcément
  un client. Cependant, la partie "Besoins" évoquent les RendezVous par Client ou Animal et l'énoncé ne dit pas
  qu'un RendezVous est lié à un Client mais plutôt à un Animal d'où notre décision.
  Un Animal possède un propriétaire mais un même propriétaire peut possèder plusieurs animaux (association 1 - *).

- Possède une race et une espèce. Nous aurions pu mettre ces éléments en tant qu'attributs. Cependant, ils seront
  utilisés pour calculer le prix d'une prestation et la manière dont cela est fait n'est pas claire. En prévision,
  nous avons fait le choix de créer deux classes. 
  Un animal possède forcément une race / espèce et une même race / espèce peut être liée à plusieurs animaux (association 1 - *)

- Est lié à un Vétérinaire par un Rendez-vous. Un même vétérinaire peut s'occuper de plusieurs animaux (association 1 - *)
```

## Race

```
- Possède un nom qui l'identifie (clé).
```

## Espèce

```
- Possède un nom qui l'identifie (clé).
```

## RendezVous

```
- Est toujours lié à une prestation (association 1 - 1).
- Concerne un animal et un vétérinaire.
```

## Produit

```
- Est soit un médicament soit un produit d'entretien. N'existe pas seule et est donc abstraite.
- Héritage exclusif entre les classes filles.
- Possède un numéro de série qui l'identifie (clé), un nom et une date de fabrication.
```

## ProduitEntretien

```
- Héritage avec la classe Produit.
```

## Medicament 

```
- Héritage avec la classe Produit.
```

## Facture

```
- Peut-être liée à des produits (association * - *) en spécifiant une quantité (classe association Vente).
- Cette association n'est pas une composition car les produits existent indépendamment de la facture.

- Peut-être liée à plusieurs prestations (association * - *).
- L'association est une composition car nous avons fait le choix qu'une prestation n'existe pas sans être liée à une facture.

- Référence un employé (association 1 - * avec la classe Employé)
- Concerne un animal (association 1 - * avec la classe Animal)

- Les attributs montant, date et moyen_paiement sont optionels car renseignés au moment du paiement.
- Choix du moyen de paiement en tant que type énuméré.
```

## Vente

```
- Classe portant sur l'association entre Produit et Facure.
- Contient la quantité vendue qui est positive.
```

## Prestation

```
- Est soit une Consultation soit une Intervention. N'existe pas seule (classe abstraite).
- Possède un prix qui est un réel positif. Comme le prix edépend de l'espèce / la race selon la classe
  fille et que rien n'est indiqué concernant son calcul (qui peut être complexe), son calcul sera délégué
  à la partie applicative.
```

## Consultation

```
- Héritage avec la classe Prestation.
```

## Intervention

```
- Héritage avec la classe Prestation.
```

# Eléments additionnels

L'extraction des statistiques sera gérée du côté de l'application.

# Points à clarifier :

## Classe Animal 

- Pour les champs de la classe Animal, l'énoncé mentionne "etc.". Nous ne pouvons pas laisser de place au doute
  dans le MCD et nous souhaiterions une clarification sur les autres attributs susceptibles d'être utilisés.

- Un Animal peut-il exister seule dans la base, sans être lié à un Vétérinaire ? Cela impactera les cardinalités
  de l'association.

## Classe RendezVous

- L'énoncé ne dit rien sur les attributs de cette classe. Par défaut, nous n'avons rien mis mais il serait
  judicieux selon nous de spécifier une date.

## Classes Client, Vétérinaire, Employé

- Une même Personne peut-elle occuper plusieurs rôle c'est-à-dire être à la fois un Client et un Employé ou
  un Client et un Vétérinaire ? Cela aura un impact sur le type d'héritage (exclusif ou non exclusif).

## Classes Prestation / Consultation / Intervention

- Comment est calculé le prix en fonction de l'espèce / race de l'Animal dans les classes filles ? S'il est fixe
  on peut penser à ajouter un champ prix dans les classes Espèce et Race (d'où l'idée d'avoir prévu des classes).
  Sinon, on délèguera sa gestion à la partie applicative.