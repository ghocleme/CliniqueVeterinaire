# MLD - Projet Clinique Vétérinaire

Groupe : GHO Clément, JOY Matthew, LE MOUEL Yacine

## MLD

```
Veterinaires(#id:int, nom:string, prenom:string) with {nom, prenom not null}

Employes(#id:int, nom:string, prenom:string) with {nom, prenom not null}

Clients(#telephone:string, nom:string, prenom:string) with {nom, prenom not null}

Animaux(#id:int, code:int, nom:string, poids:float, genre:{M,F}, date_naissance:date, taille:int, proprietaire=>Clients.telephone, race=>Races.nom, espece=>Especes.nom) with {nom, poids, genre, date_naissance, proprietaire, race, espece not null}, {code unique}, {taille, poids > 0}

Especes(#nom:string)

Races(#nom:string, espece=>Especes.nom) with {espece not null}

RendezVous(#id:int, date:date, veterinaire=>Veterinaires.id, animal=>Animaux.id) with {date, veterinaire, animal, not null}

Factures(#id:int, montant:float, date:date, moyen_paiement:{carte, cheque, espece}, editeur=>Employes.id, animal=>Animaux.id) with {editeur, animal not null}, {montant > 0}

Prestations(#id:int, #facture=>Factures.id, rdv=>RendezVous.id, prix:float, type:{Consultation, Intervention}) with {prix, rdv, type not null}, {prix > 0}

Produits(#numero_serie:int, nom:string, date_fabrication:date, type:{ProduitEntretien, Medicament}) with {nom, date_fabrication, type not null}

ProduitsFactures(#facture=>Factures.id, #produit=>Produits.numero_serie)
```

## Contraintes

```
Projection(Animaux, proprietaire) = Projection(Clients, id)
```

## Justification héritages

Héritage Personne / Vétérinaire / Client / Employé
```
La classe mère Personne est abstraite donc l'héritage par référence est inadapté. L'héritage n'est pas complet car les classes filles ont des caractéristiques et des associations ce qui rend l'héritage par classe mère également inadapté. On utilisera donc un héritage par classe fille dans ce cas. 
```

Héritage Produit / ProduitEntretien / Medicament
```
La classe mère Produit possède une association * - * ce qui rend l'héritage par classes filles inadapté. La classe mère Produit est abstraite, ce qui rend l'héritage par référence également inadapté. On utilisera donc un héritage par classe mère dans ce cas, qui fonctionnera d'ailleurs très bien étant donné que l'héritage est complet.
```

Héritage Prestation / Consultation / Intervention
```
La classe mère Prestation est abstraite, ce qui rend l'héritage par référence inadapté. Comme l'héritage est complet, on va ici privilégier un héritage par classe mère qui sera plus simple à mettre en oeuvre.
```